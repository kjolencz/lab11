package edu.towson.cosc431.labsapp

import android.graphics.Bitmap
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import java.lang.Exception
import java.net.URL
import javax.security.auth.callback.Callback

class NetworkHelper {

    companion object{
        val API_URL = "https://my-json-server.typicode.com/rvalis-towson/lab_api"
    }

    fun fetchPeople(callback: (List<Person>) -> Unit){
        AndroidNetworking.get(API_URL)
            .setTag(this)
            .setPriority(Priority.LOW)
            .build()
            .getAsObjectList(Person::class.java, object: ParsedRequestListener<List<Person>>{
                override fun onResponse(response: List<Person>?) {
                    if(response != null){
                        callback(response)
                    }else{
                        throw Exception("Error fetching people")
                    }
                }

                override fun onError(anError: ANError?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
    }
    fun fetchIcon(iconURL: String, callback: (Bitmap) -> Unit){
        AndroidNetworking.get(iconURL)
            .setTag("ImageRequestTag")
            .setPriority(Priority.LOW)
            .setBitmapMaxHeight(400)
            .setBitmapMaxWidth(400)
            .setBitmapConfig(Bitmap.Config.ARGB_8888)
            .build()
            .getAsBitmap(object: BitmapRequestListener{
                override fun onResponse(response: Bitmap?) {
                    if(response != null){
                        callback(response)
                    }else{
                        throw Exception("Error fetching icon")
                    }
                }

                override fun onError(anError: ANError?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })
    }
}