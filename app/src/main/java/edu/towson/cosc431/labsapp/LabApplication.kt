package edu.towson.cosc431.labsapp

import android.app.Application
import com.androidnetworking.AndroidNetworking

class LabApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidNetworking.initialize(this)

    }
}