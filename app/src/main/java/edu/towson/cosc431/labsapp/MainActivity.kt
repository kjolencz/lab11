package edu.towson.cosc431.labsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.graphics.Bitmap
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.person_item.*

class MainActivity : AppCompatActivity(), IController {
    override fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit) {
        NetworkHelper()
            .fetchIcon(iconUrl, callback)
    }

    companion object {
        val TAG = MainActivity::class.java.simpleName
    }

    val mutablePeople: MutableList<Person> = mutableListOf()
    override val people: List<Person> = mutablePeople

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        NetworkHelper()
            .fetchPeople {people ->
                mutablePeople.clear()
                mutablePeople.addAll(people)
                recyclerView.adapter?.notifyDataSetChanged()
            }


        recyclerView.adapter = PersonAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}

interface IController {
    fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit)
    val people: List<Person>
}